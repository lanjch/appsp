# 项目介绍

## 文件结构

### 后端结构
```
com.ruoyi
├── sp-app      // APP 接口调用模块 启动类模块
│       └── controller                    // controller层
│       └── mapper                    // mapper
│       └── model                    // 模型
│       └── service                    // 接口、实现
│       └── SpAppApplication                    // 启动类
├── sp-auth      // 权限相关模块
│       └── aspect                    //切面相关：包含行为日志切面，权限切面，自定义权限注解
│       └── config                    //配置相关
│             └── CorsConfig                    //跨域配置
│             └── DruidConfig                    //数据库配置
│             └── ExceptionConfig                    //异常配置
│             └── MybatisPlusConfig                    //MybatisPlus配置类 分页拦截器
│             └── MybatisPlusConfig                    //MybatisPlus配置类 分页拦截器
│             └── RedissonConfig                    //Redisson 配置
│             └── SecurityConfig                    //Security 配置
│             └── SwaggerConfig                    //Swagger2的接口配置
│       └── controller                    // controller层
│       └── enums                    // 枚举配置
│       └── exception                    // 自定义异常
│       └── filter                    // 拦截器 token过滤器 验证token有效性
│       └── mapper                    // mapper
│       └── model                    // 模型
│       └── service                    // 接口、实现
│       └── util                    // sp-auth 工具
├── sp-common      // 公共模块
│
│
├── sp-notice      // 公告模块
│       └── controller                    // controller层
│       └── mapper                    // mapper
│       └── model                    // 模型
│       └── service                    // 接口、实现
├── sp-version      // 版本更新模块
│       └── controller                    // controller层
│       └── mapper                    // mapper
│       └── model                    // 模型
│       └── service                    // 接口、实现

```
#### 目录关联
```
sp-common ── > sp-auth ──────── > sp-notice   ──────── > sp-app
                   └─────────── > sp-version  ──────── > sp-app
```

### 前端结构
```
├── build                      // 构建相关
├── config                     //接口配置相关
│   ├── dev.env.js                    // 开发环境配置
│   ├── test.env.js                    // 测试环境配置
│   ├── uat.env.js                    // uat环境配置
│   ├── prod.env.js                    // 生产环境配置
│   ├── index.js                    //
├── src                        // 源代码
│   ├── api                    // 所有请求
│   ├── assets                 // 主题 字体等静态资源
│   ├── components             // 全局公用组件
│   ├── icons                  // 图标
│   ├── router                 // 路由
│   ├── store                  // 全局 store管理
│   ├── styles                  // 样式
│   ├── utils                  // 全局公用方法
│   ├── views                  // view
│   ├── App.vue                // 入口页面
│   ├── main.js                // 入口 加载组件 初始化等
│   ├── permission.js          // 权限管理
├── .gitignore                 // git 忽略项
├── package.json               // package.json
└── vue.config.js              // vue.config.js
```
## 配置文件
通用配置 `application-dev.properties`

``` java
spring.application.name=sp-service
server.servlet.context-path=/sp
server.port=8081

# druid sql 密码加密
#java -cp druid-1.x.xx.jar com.alibaba.druid.filter.config.ConfigTools "Monitor_321"
spring.datasource.druid.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.druid.url=jdbc:mysql://127.0.0.1:3306/open_app_sp_service_platform?useUnicode=true&characterEncoding=utf8&autoReconnect=true&useSSL=false&useTimezone=true&serverTimezone=GMT%2B8
spring.datasource.druid.username=root
spring.datasource.druid.password=root
# mysql 密码 加密 与 sp-auth模块下DruidConfig中IDataSourceProperties
#spring.datasource.druid.password=XYmzL3ejmr9zr6aYoMdPM7gopBSCbipbsRvmv8EzNW+XU8TVK5l9WWBycvtbqJYZ/PXBk9p1eM93J/2QIl/m6g==
#spring.datasource.druid.public-key=MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMgGqqLu2pIr04HbwY/0QzmPkTDUonz7EW/8QnZNYaWZ9DX54vRBOVxHnaqI6Kdf+NFZqOOEF9rWPJG+z4IVHRUCAwEAAQ==
#spring.datasource.druid.connection-properties=config.decrypt=true;config.decrypt.key=${public-key}
#spring.datasource.druid.filter.config.enabled=true

#spring.datasource.type=com.alibaba.druid.pool.DruidDataSource
spring.datasource.druid.initial-size=1
spring.datasource.druid.max-active=100
spring.datasource.druid.min-idle=1
spring.datasource.druid.max-wait=30000
spring.datasource.druid.keep-alive=true
spring.datasource.druid.phy-timeout-millis=1200000
spring.datasource.druid.validation-query="select 1"
spring.datasource.druid.validation-query-timeout=10000
spring.datasource.druid.test-on-borrow=false
spring.datasource.druid.test-on-return=false
spring.datasource.druid.test-while-idle=true
spring.datasource.druid.time-between-eviction-runs-millis=10000
spring.datasource.druid.web-stat-filter.enabled=false
spring.datasource.druid.stat-view-servlet.enabled=false

#redis
#单机模式
#spring.redis.database=1
spring.redis.host=127.0.0.1
spring.redis.port=6379
spring.redis.password=

# 集群模式
#spring.redis.database=1
#spring.redis.cluster.nodes=10.108.0.1:6379,10.108.0.2:6379
#spring.redis.password=123456

# 哨兵模式
#spring.redis.database=1
#spring.redis.sentinel.master=master01
#spring.redis.sentinel.nodes=10.108.0.1:26379,10.108.0.2:2679,10.108.0.3:26379
#spring.redis.password=123456

spring.redis.timeout=10000
spring.redis.lettuce.pool.min-idle=0
spring.redis.lettuce.pool.max-idle=8
spring.redis.lettuce.pool.max-active=8
spring.redis.lettuce.pool.max-wait=-1ms

mybatis-plus.mapper-locations=classpath*:mapper/*.xml
mybatis-plus.type-aliases-package=com.anji.sp.model.po
#token header key
token.header=Authorization
# token secret key
token.secret=secret123456secret
#token失效默认30分钟 60*24
token.expireTime=30

swagger.enabled=true
swagger.pathMapping=/

# 分页配置
pagehelper.helperDialect=mysql
pagehelper.reasonable=true
pagehelper.supportMethodsArguments=true
pagehelper.params=count=countSql


#apk 下载路径前缀
file.apk.url=https://openappsp.anji-plus.com/download/
# 上传你文件地址  upload.filename + /apk  查看 sp-version util FileProfileConfig、FileNameEnum、FileUploadUtils
upload.filename=/app/file-sp
#debuge mac file
#upload.filename=/Users/
#debuge windows file
#upload.filename=D://
spring.servlet.multipart.max-file-size=500MB
spring.servlet.multipart.max-request-size=500MB
#APP是否开启加密请求
version.need.decrypt=true
#默认毫秒数 7 天 7*24*60*60*1000=604800000
version.timestamp.default=604800000
#默认毫秒数 1 天 1*24*60*60*1000=86400000
version.timestamp.one=86400000



#缓存local/redis  https://gitee.com/anji-plus/captcha.git
aj.captcha.cache-type=redis
#local缓存的阈值,达到这个值，清除缓存
#aj.captcha.cache-number=1000
#local定时清除过期缓存(单位秒),设置为0代表不执行
#aj.captcha.timing-clear=180
aj.captcha.type=blockpuzzle
#右下角水印文字(安吉加加)
aj.captcha.water-mark=ANJI-PLUS
#右下角水印字体(宋体)
aj.captcha.water-font=\u5b8b\u4f53
#点选文字验证码的文字字体(宋体)
aj.captcha.font-type=\u5b8b\u4f53
#校验滑动拼图允许误差偏移量(默认10像素)
aj.captcha.slip-offset=5
#aes加密坐标开启或者禁用(true|false)
aj.captcha.aes-status=true
#滑动干扰项(0/1/2) 1.2.2版本新增
aj.captcha.interference-options=1

#actuator 默认关闭所有endpoint，只打开health。
management.endpoints.enabled-by-default=false
management.endpoint.health.enabled=true
management.endpoint.health.show-details=always
management.health.diskspace.enabled=true
management.health.diskspace.threshold=10737418240
management.health.db.enabled=true
management.health.redis.enabled=true
management.health.elasticsearch.enabled=true
```

## 核心技术
### 后端技术
#### SpringBoot框架
1. 介绍

Spring Boot是由Pivotal团队提供的全新框架，其设计目的是用来简化新Spring应用的初始搭建以及开发过程。该框架使用了特定的方式来进行配置，从而使开发人员不再需要定义样板化的配置。通过这种方式，Spring Boot致力于在蓬勃发展的快速应用开发领域(rapid application development)成为领导者。
`Spring Boot2.0 环境要求必须是jdk8或以上版本，Tomcat8或以上版本`

2. 优势
- 配置 web.xml，加载 Spring 和 Spring mvc
- 配置数据库连接、配置 Spring 事务
- 配置加载配置文件的读取，开启注解
- 配置日志文件
- 配置完成之后部署 Tomcat 调试
- 自动配置、快速构建项目、快速集成新技术能力 没有冗余代码生成和XML配置的要求
- 自带项目监控

#### Spring Security安全控制
1. 介绍

   Spring Security是一个能够为基于Spring的企业应用系统提供声明式的安全访问控制解决方案的安全框架。

2. 功能
- Authentication： 认证，就是用户登录
- Authorization： 授权，判断用户拥有什么权限，可以访问什么资源
- 安全防护，跨站脚本攻击，session攻击等
- 非常容易结合Spring进行使用

#### ip2region ip地址查询
gitee地址： [https://gitee.com/lionsoul/ip2region](https://gitee.com/lionsoul/ip2region)
1. 介绍

准确率99.9%的离线IP地址定位库，0.0x毫秒级查询，ip2region.db数据库只有数MB，提供了java,php,c,python,nodejs,golang,c#等查询绑定和Binary,B树,内存三种查询算法。<br/>
主要用来处理数据请求ip解析，便于APP SDK 用户统计

2. 功能
- 每条ip数据段都固定了格式：`_城市Id|国家|区域|省份|城市|ISP_`
- 体积小
- 查询速度快，内置了三种查询算法
    * memory算法
    * binary算法
    * b-tree算法


### 前端技术
- npm：node.js
- vue-cli：Vue的脚手架工具
- vue-router： Vue提供的前端路由工具
- vuex：Vue提供的状态管理工具
- element-ui：基于MVVM框架Vue开源出来的一套前端ui组件
