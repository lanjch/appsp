//
//  ViewController.m
//  AJPushDemo
//
//  Created by Black on 2021/2/3.
//  Copyright © 2018 anji-plus 安吉加加信息技术有限公司 http://www.anji-plus.com. All rights reserved.
//

#import "ViewController.h"
#import "SettingController.h"
#import "RequestManager.h"
#import "SettingManager.h"
#import <JPush/JPUSHService.h>

#import <AJPushSDK/AJPushSDK.h>
#import "Toast.h"
#import "AJScrollView.h"

@interface ViewController ()<UIScrollViewDelegate, UITextFieldDelegate,UITextViewDelegate>
@property (nonatomic, strong) UITextView *textview;
@property (nonatomic, strong) AJScrollView *scrollView;

@property (nonatomic, strong) UITextField *titleText;
@property (nonatomic, strong) UITextField *contentText;
@property (nonatomic, strong) UITextField *pushIdText;

@property (nonatomic, strong) UIButton *pushButton;
@property (nonatomic, strong) UIButton *passthroughButton;
@property (nonatomic, strong) UIToolbar * toobar;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1]];
    self.title = @"AJPushDemo";
    
    
    [self buildSetting];
    [self setUI];
    //添加推送通知
    [self initNSNotificationCenter];
    [self registerForKeyboardNotifications];
}

- (void)setUI {
    CGRect bounds = [[UIScreen mainScreen] bounds];
    _toobar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 38.0f)];
    _toobar.translucent = YES;
    _toobar.barStyle = UIBarStyleDefault;
    UIBarButtonItem * spaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem * doneBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard)];
    [_toobar setItems:@[spaceBarButtonItem,doneBarButtonItem]];
    
    self.scrollView = [[AJScrollView alloc]initWithFrame:bounds];
    self.scrollView.backgroundColor = UIColor.orangeColor;
    [_scrollView setDelegate:self];
    
    UILabel *toastLabel = [[UILabel alloc]init];
    toastLabel.frame = CGRectMake(10, 20, bounds.size.width-20, 1);
    toastLabel.textColor = [UIColor redColor];
    [self.scrollView addSubview:toastLabel];
    
    UILabel *label = [[UILabel alloc]init];
    label.frame = CGRectMake(10, CGRectGetMaxY(toastLabel.frame) + 20, 80, 30);
    label.text = @"标题：";
    [self.scrollView addSubview:label];
    self.titleText = [[UITextField alloc]init];
    self.titleText.frame = CGRectMake(CGRectGetMaxX(label.frame) + 5, CGRectGetMaxY(toastLabel.frame) + 20, bounds.size.width-CGRectGetMaxX(label.frame) - 20, 30);
    self.titleText.placeholder = @"请输入推送/透传标题";
    self.titleText.text = @"iOS发送推送/透传标题";
    self.titleText.clearButtonMode=UITextFieldViewModeWhileEditing;
    self.titleText.delegate=self;
    self.titleText.backgroundColor = [UIColor whiteColor];
    self.titleText.inputAccessoryView = _toobar;
    [self.scrollView addSubview:self.titleText];
    
    
    UILabel *label1 = [[UILabel alloc]init];
    label1.frame = CGRectMake(10, CGRectGetMaxY(self.titleText.frame)+20, 80, 30);
    label1.text = @"内容：";
    [self.scrollView addSubview:label1];
    //
    self.contentText = [[UITextField alloc]init];
    self.contentText.frame = CGRectMake(CGRectGetMaxX(label1.frame) + 5, CGRectGetMaxY(self.titleText.frame)+20, bounds.size.width-CGRectGetMaxX(label.frame) - 20, 30);
    self.contentText.placeholder = @"请输入推送/透传内容";
    self.contentText.text = @"iOS发送推送/透传内容";
    self.contentText.delegate=self;
    self.contentText.clearButtonMode=UITextFieldViewModeWhileEditing;
    self.contentText.backgroundColor = [UIColor whiteColor];
    self.contentText.inputAccessoryView = _toobar;
    [self.scrollView addSubview:self.contentText];
    
    
    UILabel *label2 = [[UILabel alloc]init];
    label2.frame = CGRectMake(10, CGRectGetMaxY(self.contentText.frame)+20, 80, 30);
    label2.text = @"推送id：";
    [self.scrollView addSubview:label2];
    
    self.pushIdText = [[UITextField alloc]init];
    self.pushIdText.frame = CGRectMake(CGRectGetMaxX(label2.frame) + 5, CGRectGetMaxY(self.contentText.frame)+20, bounds.size.width-CGRectGetMaxX(label2.frame) - 20 - 60, 30);
    self.pushIdText.placeholder = @"请输入推送id，默认本机推送id";
    self.pushIdText.text = [JPUSHService registrationID];
    self.pushIdText.clearButtonMode=UITextFieldViewModeWhileEditing;
    self.pushIdText.inputAccessoryView = _toobar;

    self.pushIdText.delegate=self;
    self.pushIdText.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:self.pushIdText];
    
    UIButton *copyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    copyButton.frame = CGRectMake(CGRectGetMaxX(self.pushIdText.frame) + 10, CGRectGetMaxY(self.contentText.frame) + 20, 60, 30);
    copyButton.backgroundColor = [self colorR:100 g:149 b:237];
    [copyButton addTarget:self action:@selector(copyButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [copyButton setTitle:@"复制" forState:UIControlStateNormal];
    [self.scrollView addSubview:copyButton];
    
    
    UILabel *label22 = [[UILabel alloc]init];
    label22.frame = CGRectMake(10, CGRectGetMaxY(self.pushIdText.frame)+20, bounds.size.width - 20, 60);
    label22.text = @"Tip: 可以尝试获取其他手机的推送id进行推送测试(将推送id复制给其他手机)";
    label22.numberOfLines = 2;
    label22.textColor = [UIColor redColor];
    [self.scrollView addSubview:label22];
    [label22 setNeedsLayout];
    
    UILabel *label3 = [[UILabel alloc]init];
    label3.frame = CGRectMake(10, CGRectGetMaxY(label22.frame)+20, 200, 30);
    label3.text = @"推送/透传结果：";
    [self.scrollView addSubview:label3];
    
    self.textview = [[UITextView alloc] init];
    self.textview.frame = CGRectMake(10, CGRectGetMaxY(label3.frame)+20, bounds.size.width-20, 100);
    self.textview.textColor = [UIColor blueColor];
    self.textview.delegate = self;
    self.textview.inputAccessoryView = _toobar;
    [_scrollView addSubview:self.textview];
    
    //整体再将ScrollerView显示在窗口中
    [self.view addSubview:_scrollView];
    [_scrollView setContentSize:CGSizeMake(bounds.size.width, bounds.size.height*1.2)];
    
    
    self.pushButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.pushButton addTarget:self action:@selector(pushButtonClick) forControlEvents:UIControlEventTouchUpInside];
    self.pushButton.backgroundColor = [self colorR:100 g:149 b:237];
    [self.pushButton setTitle:@"推送测试" forState:UIControlStateNormal];
    
    self.passthroughButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.passthroughButton addTarget:self action:@selector(passthroughButtonClick) forControlEvents:UIControlEventTouchUpInside];
    self.passthroughButton.backgroundColor = [self colorR:100 g:149 b:237];
    
    [self.passthroughButton setTitle:@"透传测试" forState:UIControlStateNormal];
    
    UIStackView *subStackView = [[UIStackView alloc] init];
    //子视图之间的间距
    subStackView.spacing = 10.0;
    //子视图的对齐方式。
    subStackView.alignment = UIStackViewAlignmentFill;
    //stackView排列方向 横向排列
    subStackView.axis = UILayoutConstraintAxisHorizontal;
    //子视图的宽度或高度布局。
    subStackView.distribution = UIStackViewDistributionFillEqually;
    [subStackView addArrangedSubview:self.pushButton];
    [subStackView addArrangedSubview:self.passthroughButton];
    subStackView.frame = CGRectMake(30, CGRectGetMaxY(self.textview.frame) + 40, self.view.bounds.size.width -60, 40);
    [_scrollView addSubview:subStackView];
    
}

- (void)copyButtonClick {
    if (self.pushIdText.text.length == 0) {
        [self setToast:@"推送Id不存在"];
        return;
    }
    UIPasteboard *appPasteBoard =  [UIPasteboard generalPasteboard];
    [appPasteBoard setString:self.pushIdText.text];
    [self setToast:@"复制成功"];
    
}

- (void)resignKeyboard{
    
    [self.view endEditing:YES];
}

//推送按钮
-(void)pushButtonClick{
    if (self.titleText.text.length == 0) {
        [self setToast:@"请输入标题"];
        return;
    }
    if (self.contentText.text.length == 0) {
        [self setToast:@"请输入内容"];
        return;
    }
    if (self.pushIdText.text.length == 0) {
        [self setToast:@"推送Id不存在"];
        return;
    }
    NSMutableDictionary *dic = @{
        @"appKey": [SettingManager getAppSpKey],
        @"secretKey": [SettingManager getAppSpSecretKey],
        @"title":self.titleText.text,
        @"content":self.contentText.text,
        @"pushType":@"0",
        @"deviceIds":@[self.pushIdText.text],
        @"extras":@{
                @"AJPush":@"1234",
                @"key":@"test"
        },
        @"iosConfig":@{
                @"sound":@"aa.caf"
        },
        @"androidConfig":@{
                @"sound":@"aa.caf"
        }
    }.mutableCopy;
    [[RequestManager sharedInstance]POSTRequestWithUrl:[NSString stringWithFormat:@"%@/sp/push/pushBatch", [SettingManager getAppSpUrl]] paramaters:dic successBlock:^(id  _Nonnull object, NSURLResponse * _Nonnull response) {
        if([object[@"repCode"]  isEqual: @"0000"]){
            self.textview.text = @"推送成功";
        }else {
            self.textview.text = [NSString stringWithFormat:@"%@\n%@",@"推送失败",[self stringConvert:object[@"repMsg"]]];
        }
    } FailBlock:^(NSError * _Nonnull error) {
        dispatch_queue_t queue = dispatch_get_main_queue();
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), queue, ^{
            self.textview.text = [NSString stringWithFormat:@"%@\n%@",@"推送失败", error.description];
        });
    }];
    
}

//透传按钮
-(void)passthroughButtonClick{
    if (self.titleText.text.length == 0) {
        [self setToast:@"请输入标题"];
        return;
    }
    if (self.contentText.text.length == 0) {
        [self setToast:@"请输入内容"];
        return;
    }
    if (self.pushIdText.text.length == 0) {
        [self setToast:@"推送Id不存在"];
        return;
    }
    NSMutableDictionary *dic = @{
        @"appKey": [SettingManager getAppSpKey],
        @"secretKey": [SettingManager getAppSpSecretKey],
        @"title":self.titleText.text,
        @"content":self.contentText.text,
        @"pushType":@"1",
        @"deviceIds":@[self.pushIdText.text],
        @"extras":@{
                @"AJPush":@"1234",
                @"key":@"test"
        },
        @"iosConfig":@{
                @"sound":@"aa.caf"
        },
        @"androidConfig":@{
                @"sound":@"aa"
        }
    }.mutableCopy;
    [[RequestManager sharedInstance]POSTRequestWithUrl:[NSString stringWithFormat:@"%@/sp/push/pushBatch", [SettingManager getAppSpUrl]] paramaters:dic successBlock:^(id  _Nonnull object, NSURLResponse * _Nonnull response) {
        if([object[@"repCode"]  isEqual: @"0000"]){
            self.textview.text = @"透传成功";
        }else {
            self.textview.text = [NSString stringWithFormat:@"%@\n%@",@"透传失败",[self stringConvert:object[@"repMsg"]]];
        }
    } FailBlock:^(NSError * _Nonnull error) {
        dispatch_queue_t queue = dispatch_get_main_queue();
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), queue, ^{
            self.textview.text = [NSString stringWithFormat:@"%@\n%@",@"透传失败", error.description];
        });
    }];
}
//创建配置中心入口
- (void)buildSetting {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"setting"] style:UIBarButtonItemStylePlain target:self action:@selector(settingClick)];
}
- (void)settingClick {
    SettingController *setVc = [[SettingController alloc] init];
    [self.navigationController pushViewController:setVc animated:YES];
}

//注册推送通知中心
- (void)initNSNotificationCenter {
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(noticeBarMessage:)
                          name:kAJPushServiceNoticeBarMessageNotification
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(innerMessage:)
                          name:kAJPushServiceInnerMessageNotification
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(customMessage:)
                          name:kAJPushServiceCustomMessageNotification
                        object:nil];
}
//注册键盘监听
- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}
//键盘弹出代理
- (void)keyboardWillShow:(NSNotification *)aNotification {
    NSDictionary* info = [aNotification userInfo];
    // 注意不要用UIKeyboardFrameBeginUserInfoKey，第三方键盘可能会存在高度不准，相差40高度的问题
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    // 修改滚动天和tableView的contentInset
    self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    self.scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    //获取当前文本框架大小
    UIView *v = [UIView new];
    
    if (self.titleText.isFirstResponder) {
        v = self.titleText;
    }
    if (self.contentText.isFirstResponder) {
        v = self.contentText;
    }
    if (self.pushIdText.isFirstResponder) {
        v = self.pushIdText;
    }
    if (self.textview.isFirstResponder) {
        v = self.textview;
    }
    CGRect textFieldRect = [v frame];
    //滚动到当前文本框
    [self.scrollView scrollRectToVisible:textFieldRect animated:YES];
    
}
//键盘隐藏代理
- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    self.scrollView.contentInset = UIEdgeInsetsZero;
    self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

// textfield代理
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
// textView代理
- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
    }
    return YES;
}

//消息栏通知响应通知
- (void)noticeBarMessage:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSLog(@"++++++++++noticeBarMessage++++++++++++++");
    NSLog(@"%@",userInfo);
    [self showMessageInfo:userInfo delay:0.5];
    NSLog(@"++++++++++noticeBarMessage++++++++++++++");
}

//应用内部响应通知
- (void)innerMessage:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSLog(@"+++++++++++innerMessage+++++++++++++");
    NSLog(@"%@",userInfo);
    NSString *title = userInfo[@"aps"][@"alert"][@"title"];
    NSString *body = userInfo[@"aps"][@"alert"][@"body"];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:body preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *conform = [UIAlertAction actionWithTitle:@"查看" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //处理跳转
        [self showMessageInfo:userInfo delay:0.1];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消查看" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:conform];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    NSLog(@"+++++++++++innerMessage+++++++++++++");
}

//自定义消息响应通知
- (void)customMessage:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSLog(@"+++++++++++customMessage+++++++++++++");
    NSLog(@"%@",userInfo);
    NSString *body = userInfo[@"content"];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"自定义消息" message:body preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *conform = [UIAlertAction actionWithTitle:@"查看" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //处理跳转
        [self showMessageInfo:userInfo delay:0.1];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消查看" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:conform];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    NSLog(@"+++++++++++customMessage+++++++++++++");
}

//页面显示通知消息详情
- (void)showMessageInfo:(NSDictionary *)info delay:(NSTimeInterval)time {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:info options:NSJSONWritingPrettyPrinted error:NULL];
    NSString *str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    dispatch_queue_t queue = dispatch_get_main_queue();
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(time * NSEC_PER_SEC)), queue, ^{
        self.textview.text = [NSString stringWithFormat:@"%@", str];
        [self.textview scrollRectToVisible:CGRectMake(0, self.textview.contentSize.height-15, self.textview.contentSize.width, 10) animated:YES];
    });
}
//屏幕手势事件
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

// 转UTF8
- (NSString *)convert:(id)dic {
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    if (![jsonData isKindOfClass:[NSData class]]) {
        return nil;
    }
    NSString *tmp=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return tmp;
}

// string转UTF8
- (NSString *)stringConvert:(NSString*)dic {
    
    NSData* jsonData = [dic dataUsingEncoding:NSUTF8StringEncoding];
    if (![jsonData isKindOfClass:[NSData class]]) {
        return nil;
    }
    NSString *tmp=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return tmp;
}


//异常toast
- (void)setToast:(NSString*)toast{
    [[Toast makeText:toast] showWithType:AJShortTime];
}

- (UIColor *)colorR:(CGFloat)r g:(CGFloat)g b:(CGFloat)b {
    return  [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1];
}
@end
