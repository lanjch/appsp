package com.anji.sp.enums;

/**
 * is delete
 */
public enum UploadFileTypeEnum {
    APK("0", "apk"), PICTURE("1", "图片");

    private final String code;
    private final String info;

    UploadFileTypeEnum(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public Integer getIntegerCode() {
        return Integer.parseInt(code);
    }

    public Long getLongCode() {
        return Long.parseLong(code);
    }


    public String getInfo() {
        return info;
    }
}
