package com.anji.sp.push.mapper;

import com.anji.sp.push.model.po.PushHistoryPO;
import com.anji.sp.push.model.vo.PushHistoryVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 推送历史表 Mapper 接口
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
public interface PushHistoryMapper extends BaseMapper<PushHistoryPO> {

    /** XML 自定义分页
     * @param page
     * @param pushHistoryVO
     * @return
     */
    IPage<PushHistoryVO> queryByPage(Page<?> page, @Param("pushHistoryVO") PushHistoryVO pushHistoryVO);
}
