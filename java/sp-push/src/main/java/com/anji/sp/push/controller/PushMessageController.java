package com.anji.sp.push.controller;

import com.anji.sp.aspect.PreSpAuthorize;
import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.model.vo.PushMessageVO;
import com.anji.sp.push.service.PushMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 服务推送id记录 前端控制器
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
@Api(tags = "推送消息")
@RestController
@RequestMapping("/pushMessage")
public class PushMessageController {

    @Autowired
    private PushMessageService pushMessageService;


    @PostMapping("/create")
    @PreSpAuthorize("system:user:push")
    public ResponseModel create(@RequestBody PushMessageVO requestModel) {
        return pushMessageService.create(requestModel);
    }

    @PostMapping("/updateById")
    @PreSpAuthorize("system:user:push")
    public ResponseModel updateById(@RequestBody PushMessageVO requestModel) {
        return pushMessageService.updateById(requestModel);
    }

    @PostMapping("/deleteById")
    @PreSpAuthorize("system:user:push")
    public ResponseModel deleteById(@RequestBody PushMessageVO requestModel) {
        return pushMessageService.deleteById(requestModel);
    }

    @PostMapping("/queryById")
    @PreSpAuthorize("system:user:push")
    public ResponseModel queryById(@RequestBody PushMessageVO requestModel) {
        return pushMessageService.queryById(requestModel);
    }

    @PostMapping("/queryByPage")
    @ApiOperation(value = "分页查询推送消息", httpMethod = "POST")
    @PreSpAuthorize("system:user:push")
    public ResponseModel queryByPage(@RequestBody PushMessageVO requestModel) {
        return pushMessageService.queryByPage(requestModel);
    }
}

