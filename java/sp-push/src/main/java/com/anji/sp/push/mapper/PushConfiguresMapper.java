package com.anji.sp.push.mapper;

import com.anji.sp.push.model.po.PushConfiguresPO;
import com.anji.sp.push.model.vo.PushConfiguresVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 推送配置项 Mapper 接口
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
public interface PushConfiguresMapper extends BaseMapper<PushConfiguresPO> {

    /** XML 自定义分页
     * @param page
     * @param pushConfiguresVO
     * @return
     */
    IPage<PushConfiguresVO> queryByPage(Page<?> page, @Param("pushConfiguresVO") PushConfiguresVO pushConfiguresVO);
}
