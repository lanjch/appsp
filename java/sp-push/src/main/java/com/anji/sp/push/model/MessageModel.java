package com.anji.sp.push.model;

import java.io.Serializable;

/**
 * 推送消息结果类
 */
public class MessageModel implements Serializable {
    private String targetNum;//目标数
    private String successNum;//成功数
    private String statusCode;//1 成功 0 失败
    private String message;//异常信息

    public static MessageModel errorMsg(String errMsg) {
        MessageModel messageModel = new MessageModel();
        messageModel.setMessage(errMsg);
        messageModel.setStatusCode("0");
        return messageModel;
    }

    public static MessageModel errorMsg(int targetNum,int successNum,String errMsg) {
        MessageModel messageModel = new MessageModel();
        messageModel.setMessage(errMsg);
        messageModel.setSuccessNum(successNum + "");
        messageModel.setTargetNum(targetNum + "");
        messageModel.setStatusCode("0");
        return messageModel;
    }


    public static MessageModel success(int targetNum, int successNum) {
        MessageModel messageModel = new MessageModel();
        messageModel.setMessage("发送成功");
        messageModel.setStatusCode("1");
        messageModel.setSuccessNum(successNum + "");
        messageModel.setTargetNum(targetNum + "");
        return messageModel;
    }
    public static MessageModel success(int targetNum, int successNum, String message) {
        MessageModel messageModel = new MessageModel();
        messageModel.setMessage(message);
        messageModel.setStatusCode("1");
        messageModel.setSuccessNum(successNum + "");
        messageModel.setTargetNum(targetNum + "");
        return messageModel;
    }

    /**
     * 是否推送成功
     * @return
     */
    public Boolean isSuccess() {
        if (this == null) {
            return false;
        }
        return this.getStatusCode().equals("1");
    }

    public String getTargetNum() {
        return targetNum;
    }

    public void setTargetNum(String targetNum) {
        this.targetNum = targetNum;
    }

    public String getSuccessNum() {
        return successNum;
    }

    public void setSuccessNum(String successNum) {
        this.successNum = successNum;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
